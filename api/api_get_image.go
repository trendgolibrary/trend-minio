package api

import (
	"fmt"
	"github.com/labstack/echo/v4"
	requests "gitlab.com/trendgolibrary/trend-call"
)

func GetImageURL(url string) (*requests.Response, error) {
	var resRequest requests.Response
	headers := map[string]string{
		echo.HeaderContentType: "application/json",
	}

	if err := requests.Call().Get(requests.Params{
		URL:     url,
		HEADERS: headers,
		TIMEOUT: 30,
	}, &resRequest).Error(); err != nil {
		return nil, err
	}
	if resRequest.Code != 200 {
		return nil, fmt.Errorf(string(resRequest.Result))
	}

	return &resRequest, nil
}
