module gitlab.com/trendgolibrary/trend-minio

go 1.15

require (
	github.com/labstack/echo/v4 v4.3.0
	github.com/minio/minio-go/v7 v7.0.31
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/trendgolibrary/trend-call v1.0.3
)
