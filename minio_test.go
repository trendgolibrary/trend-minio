package minio

import (
	"bytes"
	"fmt"
	"github.com/minio/minio-go/v7"
	"os"
	"testing"
)

func TestUpload(t *testing.T) {
	conf := Configuration{
		Host:            "10.0.0.5:9000",
		AccessKeyID:     "cOSJ5W0QBzKPO7mL",
		SecretAccessKey: "gjgfvpmSvQhpCvih1TAobxM3GLamvcYD",
	}
	if err := NewConnection(conf); err != nil {
		panic(err)
	}
	client := GetClient()
	bkName := "miniotest"
	filepath := "./img.png"

	var responseUpload minio.UploadInfo
	if err := client.UploadImage(bkName, "img.png", filepath, &responseUpload); err != nil {
		panic(err)
	}

	PrintStructJson(responseUpload)

}

func TestUploadRender(t *testing.T) {
	conf := Configuration{
		Host:            "10.0.0.5:9000",
		AccessKeyID:     "cOSJ5W0QBzKPO7mL",
		SecretAccessKey: "gjgfvpmSvQhpCvih1TAobxM3GLamvcYD",
	}
	if err := NewConnection(conf); err != nil {
		panic(err)
	}
	client := GetClient()
	bkName := "salepagedetail"

	file, err := os.Open("./img.png")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	fileStat, err := file.Stat()
	if err != nil {
		fmt.Println(err)
		return
	}

	var responseUpload minio.UploadInfo

	if err := client.UploadImageReader(bkName, "2.png", file, fileStat.Size(), &responseUpload); err != nil {
		panic(err)
	}
	PrintStructJson(responseUpload)

}

func TestClient_GetImage(t *testing.T) {
	conf := Configuration{
		Host:            "10.0.0.5:9000",
		AccessKeyID:     "cOSJ5W0QBzKPO7mL",
		SecretAccessKey: "gjgfvpmSvQhpCvih1TAobxM3GLamvcYD",
	}
	if err := NewConnection(conf); err != nil {
		panic(err)
	}
	client := GetClient()
	bkName := "salepagedetail"

	var response bytes.Buffer
	if err := client.GetImage(bkName, "e8c02b0e-616b-4ece-bff9-11d9c53b1235.png", &response); err != nil {
		panic(err)
	}

	fmt.Println(response)
}
func TestClient_UploadImageURL(t *testing.T) {
	conf := Configuration{
		Host:            "10.0.0.5:9000",
		AccessKeyID:     "cOSJ5W0QBzKPO7mL",
		SecretAccessKey: "gjgfvpmSvQhpCvih1TAobxM3GLamvcYD",
	}
	if err := NewConnection(conf); err != nil {
		panic(err)
	}
	client := GetClient()
	bkName := "salepagedetail"

	url := `https://leanmeorganic.com/wp-content/uploads/2021/12/LEANME_SalePage_04.jpg`
	var response minio.UploadInfo
	if err := client.UploadImageURL(bkName, "e8c02b0e-616b-4ece-bff9-11d9c53b1235.png", url, &response); err != nil {
		panic(err)
	}

	fmt.Println(response)
}
