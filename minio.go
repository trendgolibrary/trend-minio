package minio

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/sirupsen/logrus"
	"gitlab.com/trendgolibrary/trend-minio/api"
	"io"
)

var (
	minioClient = &minio.Client{}
	ctx         = context.Background()
)

// Client minio client interface
type Client interface {
	UploadImage(bucketName string, objectName, filePath string, responseUpload *minio.UploadInfo) error
	UploadImageReader(bucketName string, objectName string, render io.Reader, objectSize int64, responseUpload *minio.UploadInfo) error
	UploadImageURL(bucketName string, objectName string, url string, responseUpload *minio.UploadInfo) error
	GetImage(bucketName string, objectName string, response *bytes.Buffer) error
}

// Configuration config minio for new connection
type Configuration struct {
	Host            string
	AccessKeyID     string
	SecretAccessKey string
}

// NewConnection new ftp connection
func NewConnection(config Configuration) (err error) {
	// Initialize minio client object.
	minioClient, err = minio.New(config.Host, &minio.Options{
		Creds:  credentials.NewStaticV4(config.AccessKeyID, config.SecretAccessKey, ""),
		Secure: false,
	})
	if err != nil {
		return err
	}

	return nil
}

type client struct {
	client *minio.Client
}

func (m *client) UploadImageURL(bucketName string, objectName string, url string, responseUpload *minio.UploadInfo) error {
	err := m.client.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "us-east-1", ObjectLocking: false})
	if err != nil {
		exists, errBucketExists := m.client.BucketExists(ctx, bucketName)
		if errBucketExists != nil {
			logrus.Errorf("[UploadImage] check bucket exists error: %s", err)
			return err
		}

		if !exists {
			logrus.Errorf("[UploadImage] make bucket error: %s", err)
			return err
		}
	}
	res, err := api.GetImageURL(url)
	if err != nil {
		return err
	}

	response, err := m.client.PutObject(ctx, bucketName, objectName, bytes.NewReader(res.Result), int64(len(res.Result)), minio.PutObjectOptions{})
	if err != nil {
		logrus.Errorf("[UploadImage] put object error: %s", err)
		return err
	}

	if err := CopyStructToStruct(response, responseUpload); err != nil {
		logrus.Errorf("[CopyStructToStruct] put object error: %s", err)
		return err
	}
	return nil
}

func (m *client) UploadImageReader(bucketName string, objectName string, render io.Reader, objectSize int64, responseUpload *minio.UploadInfo) error {
	err := m.client.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "us-east-1", ObjectLocking: false})
	if err != nil {
		exists, errBucketExists := m.client.BucketExists(ctx, bucketName)
		if errBucketExists != nil {
			logrus.Errorf("[UploadImage] check bucket exists error: %s", err)
			return err
		}

		if !exists {
			logrus.Errorf("[UploadImage] make bucket error: %s", err)
			return err
		}
	}

	response, err := m.client.PutObject(ctx, bucketName, objectName, render, objectSize, minio.PutObjectOptions{})
	if err != nil {
		logrus.Errorf("[UploadImage] put object error: %s", err)
		return err
	}

	if err := CopyStructToStruct(response, responseUpload); err != nil {
		logrus.Errorf("[CopyStructToStruct] put object error: %s", err)
		return err
	}

	return nil
}

func (m *client) GetImage(bucketName string, objectName string, response *bytes.Buffer) error {
	err := m.client.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "us-east-1"})
	if err != nil {
		exists, errBucketExists := m.client.BucketExists(ctx, bucketName)
		if errBucketExists != nil {
			logrus.Errorf("[UploadImage] check bucket exists error: %s", err)
			return err
		}

		if !exists {
			logrus.Errorf("[UploadImage] make bucket error: %s", err)
			return err
		}
	}

	reader, err := m.client.GetObject(ctx, bucketName, objectName, minio.GetObjectOptions{})
	if err != nil {
		logrus.Errorf("[UploadImage] put object error: %s", err)
		return err
	}

	_, err = response.ReadFrom(reader)
	if err != nil {
		return err
	}
	return nil
}

// GetClient get minio client
func GetClient() Client {
	return &client{
		client: minioClient,
	}
}

// UploadImage upload image
func (m *client) UploadImage(bucketName string, objectName, filePath string, responseUpload *minio.UploadInfo) error {
	err := m.client.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{Region: "us-east-1"})
	if err != nil {
		exists, errBucketExists := m.client.BucketExists(ctx, bucketName)
		if errBucketExists != nil {
			logrus.Errorf("[UploadImage] check bucket exists error: %s", err)
			return err
		}

		if !exists {
			logrus.Errorf("[UploadImage] make bucket error: %s", err)
			return err
		}
	}

	response, err := m.client.FPutObject(ctx, bucketName, objectName, filePath, minio.PutObjectOptions{})
	if err != nil {
		logrus.Errorf("[UploadImage] put object error: %s", err)
		return err
	}

	if err := CopyStructToStruct(response, responseUpload); err != nil {
		logrus.Errorf("[CopyStructToStruct] put object error: %s", err)
		return err
	}

	return nil
}

func CopyStructToStruct(target interface{}, result interface{}) error {
	fooByte, err := json.Marshal(&target)
	if err != nil {
		return err
	}

	err = json.Unmarshal(fooByte, result)
	if err != nil {
		return err
	}
	return nil
}

func PrintStructJson(target interface{}) {
	fooByte, _ := json.MarshalIndent(&target, "", "\t")
	fmt.Println(string(fooByte))
}
